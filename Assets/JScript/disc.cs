﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disc : MonoBehaviour
{
    public float _RotationSpeed; //定义自转的速度
    //public GameObject wave;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back * _RotationSpeed*Time.deltaTime, Space.World); //物体自转
        //this.transform.localScale = new Vector3(wave.transform.localScale.y, wave.transform.localScale.y, wave.transform.localScale.y);
    }
}
