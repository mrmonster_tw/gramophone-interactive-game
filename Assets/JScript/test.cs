﻿using UnityEngine;
using System.Collections;
using DIO_Control_ver2;

public class test : MonoBehaviour
{
    DIO_to_unity DIO = new DIO_to_unity(); //啟用DLL CLASS

    public int read; //宣告要從電路板讀的變數

    void Start()
    {
        DIO.setup();    //打開與控制板的連接通道

    }

    void Update()
    {
        read = DIO.Receive(read); //每秒刷新讀取到的值
        if (read != 0)
        {
            print(read);
        }

        if (read == 65) //可讀取65~72
        {
            this.GetComponent<Main>().start = true;
        }

        if (read == 66) //可讀取65~72
        {
            this.GetComponent<Main>().start = false;
        }

        /*if (Input.GetKey("1"))
        {
            DIO.LED_ON(1, 1);  //1號控制板的LED1亮起
            Debug.Log("aa");
        }*/
    }

    void OnApplicationQuit()
    {
        DIO.Close_DIO();    //關閉連接通道()
        //注意：換場景(scene)時一定要關閉通道
        //然後下個場景再用DIO.setup()函數開啟通道
    }


}