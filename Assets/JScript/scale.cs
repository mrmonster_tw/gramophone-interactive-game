﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scale : MonoBehaviour
{
    public RawImage pic;
    public GameObject cube;
    public GameObject self;
    public GameObject main;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<AudioSource>().isPlaying == false)
        {
            pic.enabled = false;
            pic.transform.localScale = new Vector3(1, 1, 1);
            pic.GetComponent<disc>().enabled = false;

            main.GetComponent<Main>().play = false;
            self.SetActive(false);
        }

        pic.transform.localScale = new Vector3(cube.transform.localScale.y, cube.transform.localScale.y, cube.transform.localScale.y);

        if (cube.transform.localScale.y >= 1.3f)
        {
            pic.transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
        }
        else
        {
            pic.transform.localScale = new Vector3(cube.transform.localScale.y, cube.transform.localScale.y, cube.transform.localScale.y);
        }

    }
}
