﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    Texture2D t2D;
    public RawImage pic;

    public GameObject icon_1;
    public GameObject icon_2;
    public GameObject icon_3;
    public GameObject icon_4;
    public GameObject icon_5;
    public GameObject icon_6;

    public GameObject disc_1;
    public GameObject disc_2;
    public GameObject disc_3;
    public GameObject disc_4;
    public GameObject disc_5;
    public GameObject disc_6;

    public Sprite pic_1;
    public Sprite pic_2;
    public Sprite pic_3;
    public Sprite pic_4;
    public Sprite pic_5;
    public Sprite pic_6;

    public bool play;
    public int once;
    public GameObject target;
    public GameObject target_1;

    public GameObject aim;
    public float time;

    public bool start;

    public GameObject setting;

    public float picx;
    public float picy;
    public float picscale;

    public int check;

    // Start is called before the first frame update
    void Start()
    {
        pic.GetComponent<RectTransform>().sizeDelta = new Vector2(PlayerPrefs.GetFloat("picscale"), PlayerPrefs.GetFloat("picscale"));
        pic.transform.localPosition = new Vector3(PlayerPrefs.GetFloat("picx"), PlayerPrefs.GetFloat("picy"), 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            setting.active = !setting.active;
        }
        if (target != target_1 || start == false)
        {
            if (play)
            {
                pic.enabled = false;
                pic.transform.localScale = new Vector3(1, 1, 1);
                pic.GetComponent<disc>().enabled = false;

                disc_1.SetActive(false);
                disc_2.SetActive(false);
                disc_3.SetActive(false);
                disc_4.SetActive(false);
                disc_5.SetActive(false);
                disc_6.SetActive(false);
                aim.SetActive(true);
                once = 0;
                play = false;
            }
        }


        if (start)
        {
            if (check == 0)
            {
                StartCoroutine(init());
            }
        }
    }
    IEnumerator init()
    {
        changemusic();
        check = 1;
        yield return new WaitForSeconds(1);
        check = 0;
    }
    void changemusic()
    {
        Debug.Log("aa");
        if (icon_1.activeSelf)
        {
            target = icon_1;
            if (play == false)
            {
                target_1 = icon_1;
                //StartCoroutine(scan());
                pic.texture = pic_1.texture;
                pic.enabled = true;
                pic.GetComponent<disc>().enabled = true;

                disc_1.SetActive(true);
                aim.SetActive(false);
                play = true;
            }
        }
        if (icon_2.activeSelf)
        {
            target = icon_2;
            if (play == false)
            {
                target_1 = icon_2;
                //StartCoroutine(scan());
                pic.texture = pic_2.texture;
                pic.enabled = true;
                pic.GetComponent<disc>().enabled = true;

                disc_2.SetActive(true);
                aim.SetActive(false);
                play = true;
            }
        }
        if (icon_3.activeSelf)
        {
            target = icon_3;
            if (play == false)
            {
                target_1 = icon_3;
                //StartCoroutine(scan());
                pic.texture = pic_3.texture;
                pic.enabled = true;
                pic.GetComponent<disc>().enabled = true;

                disc_3.SetActive(true);
                aim.SetActive(false);
                play = true;
            }
        }
        if (icon_4.activeSelf)
        {
            target = icon_4;
            if (play == false)
            {
                target_1 = icon_4;
                //StartCoroutine(scan());
                pic.texture = pic_4.texture;
                pic.enabled = true;
                pic.GetComponent<disc>().enabled = true;

                disc_4.SetActive(true);
                aim.SetActive(false);
                play = true;
            }
        }
        if (icon_5.activeSelf)
        {
            target = icon_5;
            if (play == false)
            {
                target_1 = icon_5;
                //StartCoroutine(scan());
                pic.texture = pic_5.texture;
                pic.enabled = true;
                pic.GetComponent<disc>().enabled = true;

                disc_5.SetActive(true);
                aim.SetActive(false);
                play = true;
            }
        }
        if (icon_6.activeSelf)
        {
            target = icon_6;
            if (play == false)
            {
                target_1 = icon_6;
                //StartCoroutine(scan());
                pic.texture = pic_6.texture;
                pic.enabled = true;
                pic.GetComponent<disc>().enabled = true;

                disc_6.SetActive(true);
                aim.SetActive(false);
                play = true;
            }
        }
    }
    public void setting_fn(int xx)
    {
        if (xx == 1)
        {
            pic.transform.localPosition = new Vector3(pic.transform.localPosition.x+1, pic.transform.localPosition.y, pic.transform.localPosition.z);
        }
        if (xx == 2)
        {
            pic.transform.localPosition = new Vector3(pic.transform.localPosition.x - 1, pic.transform.localPosition.y, pic.transform.localPosition.z);
        }
        if (xx == 3)
        {
            pic.transform.localPosition = new Vector3(pic.transform.localPosition.x, pic.transform.localPosition.y+1, pic.transform.localPosition.z);
        }
        if (xx == 4)
        {
            pic.transform.localPosition = new Vector3(pic.transform.localPosition.x, pic.transform.localPosition.y-1, pic.transform.localPosition.z);
        }

        if (xx == 5)
        {
            picscale = picscale + 5f;
            pic.GetComponent<RectTransform>().sizeDelta = new Vector2(picscale, picscale);
        }
        if (xx == 6)
        {
            picscale = picscale - 5f;
            pic.GetComponent<RectTransform>().sizeDelta = new Vector2(picscale, picscale);
        }
        if (xx == 7)
        {
            picx = pic.transform.localPosition.x;
            picy = pic.transform.localPosition.y;
            picscale = pic.GetComponent<RectTransform>().sizeDelta.x;

            PlayerPrefs.SetFloat("picx", picx);
            PlayerPrefs.SetFloat("picy", picy);
            PlayerPrefs.SetFloat("picscale", picscale);
            Application.Quit();
        }
        if (xx == 8)
        {
            picx = 0;
            picy = 0;
            pic.GetComponent<RectTransform>().sizeDelta = new Vector2(800,800);
        }
    }
    /*private IEnumerator wait_stop()
    {
        once = 1;
        yield return new WaitForSeconds(2);
        close = true;
    }
    private IEnumerator scan()
    {
        yield return new WaitForSeconds(1);
        t2D = new Texture2D(1000, 1000);
        yield return new WaitForEndOfFrame();//等待攝影機的影像繪製完畢

        t2D.ReadPixels(new Rect(450, 50, Screen.width, Screen.height), 0, 0, false);
        t2D.Apply();//開始掃描

        pic.texture = t2D;
        pic.transform.localPosition = new Vector3(0, 0, 0);

        this.GetComponent<AudioProcessor>().enabled = true;
        //pic.GetComponent<disc>().enabled = true;

        //byte[] byt = t2D.EncodeToPNG();
        //File.WriteAllBytes("E:/" + "pic" + ".png", byt);
    }*/
}
